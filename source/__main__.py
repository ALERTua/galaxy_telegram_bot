import asyncio
import logging
import os
import random
import time

import pendulum
from aiogram import Bot, types, Dispatcher, executor
from aiogram.types import ParseMode
from aiogram.types.message import ContentType
from aiogram.utils import exceptions as aiogram_exceptions
from aiogram.utils.markdown import text, escape_md
from global_logger import Log

from source.commands_static import COMMANDS
from source.tips import TIPS

# todo: на хелп отвечать в личку и удалять сообщение?
# todo: /? отвечает и в приват тоже
# todo: активу нужна помощь с: и карточки из трелло/airtable

LOG_LEVEL = os.getenv('LOG_LEVEL')  # Робат
if LOG_LEVEL:
    # noinspection PyProtectedMember
    LOG_LEVEL = Log._LOGGER_LEVELS_DICT.get(LOG_LEVEL, Log.Levels.INFO)
logging.basicConfig(level=LOG_LEVEL)
log = Log.get_logger(level=LOG_LEVEL)

MYID = os.getenv('MYID', 0)  # Робат
MAIN_CHAT_ID = os.getenv('MAIN_CHAT_ID', 0)  # Общий
AUTHOR = os.getenv('AUTHOR', '@ALERTua')
API_TOKEN = os.getenv('GALAXY_TELEGRAM_BOT')  # Робат
TG_DEBUG = os.getenv('TG_DEBUG')
if TG_DEBUG:
    API_TOKEN = os.getenv('TELEGRAM_DEBUG_BOT')  # ALERTisbot

if not API_TOKEN:
    log.exception("NO API KEY FOUND @ ENV VARIABLE GALAXY_TELEGRAM_BOT")
    # noinspection PyProtectedMember
    os._exit(1)  # noqa

log.debug(f", APIKEY: {API_TOKEN}")
log.debug(f"MYID: {MYID}")

loop = asyncio.get_event_loop()
bot = Bot(token=API_TOKEN, loop=loop)
dp = Dispatcher(bot=bot, throttling_rate_limit=5, no_throttle_error=True, loop=loop)


# @dp.message_handler(content_types=ContentType.ANY)
# async def echo_message(message: types.Message):
#     if not message.from_user.id == int(MYID):
#         return
#
#     photo = message.photo
#     if photo and len(photo):
#         await message.answer(str(photo[-1].file_id))


@dp.message_handler(content_types=ContentType.VOICE)
async def echo(message: types.Message):
    await message.delete()


@dp.message_handler(commands=['test'])
async def echo(message: types.Message):
    if not message.from_user.id == MYID:
        return

    bot_me = await bot.me
    # text = f"[{bot_me.full_name}](tg://user?id={bot_me.id})"
    text = f"[{bot_me.full_name}](mention:{bot_me.mention})"
    # await message.answer(message.text)
    await message.answer(text, parse_mode=ParseMode.MARKDOWN)


@dp.errors_handler()
async def error(update: types.Update, exception):
    if not MYID:
        return

    text = f"""{update.message.text}
from {update.message.from_user.mention}
@ {update.message.chat.type} {update.message.chat.id} : {update.message.chat.title}
resulted in exception {type(exception)}:
{exception}"""
    await update.bot.send_message(MYID, text)


async def command_base(message: types.Message):
    _alias = message.text.lstrip('/').strip()
    _media = COMMANDS.get(_alias)
    if isinstance(_media, str):
        return await message.reply(_media, disable_web_page_preview=True)

    await message.reply_media_group(_media)


for alias in COMMANDS.keys():
    dp.message_handler(commands=[alias])(command_base)


# @dp.message_handler(commands=['start'])
# async def start(message: types.Message):
#     msg = """Используй команду /help, чтобы узнать список команд."""
#     await message.reply(msg, reply=False)


@dp.message_handler(commands=['?'])
async def help_commands(message: types.Message):
    bot_me = await bot.me
    keys = list(COMMANDS.keys())
    commands = '  /' + "\n  /".join(sorted(keys))
    msg = text(
        escape_md(f'Команды {bot_me.mention} используются маленькими буквами:'),
        f'{commands}',
        f'*Пожалуйста, используйте бота в личной беседе c {bot_me.mention}, чтобы не захламлять чат.*',
        f'Если у вас есть идеи наполнения команд бота - предлагайте их {AUTHOR}.',
        sep='\n'
    )
    await message.reply(msg, parse_mode=ParseMode.MARKDOWN)


async def log_(message, dispatcher=None):
    if not MYID:
        return

    if dispatcher:
        _bot = dispatcher.bot
    else:
        _bot = bot

    try:
        await _bot.send_message(MYID, message)
    except:
        pass


async def on_startup():
    await log_("Galaxy Telegram Bot Start")


async def on_shutdown():
    await log_("Galaxy Telegram Bot Stop")


async def tip_of_the_day():
    if pendulum.now('Europe/Kiev').hour in (20,) or TG_DEBUG:
        random_message = random.choice(TIPS)
        if isinstance(random_message, str):
            return await bot.send_message(MAIN_CHAT_ID, random_message, disable_web_page_preview=True)

        await bot.send_media_group(MAIN_CHAT_ID, random_message)


def now_str():
    return pendulum.now(tz='Europe/Kiev').strftime("%Y-%m-%d %H:%M:%S")


async def repeater(async_func, delay):
    while True:
        await async_func()
        await asyncio.sleep(delay)


async def start(*args, **kwargs):
    await on_startup()
    await repeater(tip_of_the_day, 60 * 60)


if __name__ == '__main__':
    dp.loop.create_task(start())
    network_error_reported = False
    while True:
        try:
            executor.start_polling(dp, skip_updates=True, loop=loop)
        except aiogram_exceptions.Unauthorized as e:
            log.exception("Unauthorized Exception", exc_info=e)
            log_(f"{now_str()}: Unauthorized Exception {type(e)} {e}")
            os._exit(1)  # noqa
        except aiogram_exceptions.NetworkError as e:
            log.exception("Unauthorized Exception", exc_info=e)
            if not network_error_reported:
                log_(f"{now_str()}: NetworkError Exception {type(e)} {e}")
                network_error_reported = True
            time.sleep(10)
        except Exception as e:
            log.error(f"{type(e)} {e}")
            log_(f"{now_str()}: {type(e)} {e}")
            time.sleep(10)
        finally:
            if network_error_reported:
                log_(f"{now_str()}: Network restored")
                network_error_reported = False
