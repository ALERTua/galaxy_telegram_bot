#! /usr/bin/env bash

REGISTRY_IP=192.168.1.3
REGISTRY_PORT=5001
IMAGE_NAME=galaxy_telegram_bot
IMAGE_TAG=latest
BUILD_TAG=$REGISTRY_IP:$REGISTRY_PORT/$IMAGE_NAME:$IMAGE_TAG
BUILD_PATH=.
echo Build Tag: $BUILD_TAG

cd "$(dirname "$0")"
sudo service docker start
echo building with docker build -t ${BUILD_TAG} ${BUILD_PATH}
docker build -t ${BUILD_TAG} ${BUILD_PATH}

echo pushing with docker push ${REGISTRY_IP}:${REGISTRY_PORT}/${IMAGE_NAME}
docker push ${REGISTRY_IP}:${REGISTRY_PORT}/${IMAGE_NAME}


