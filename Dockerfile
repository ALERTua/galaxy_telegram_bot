FROM python:3.8.3-alpine
MAINTAINER Alexey Rubasheff <alexey.rubasheff@gmail.com>

RUN apk update && apk upgrade && \
    apk add --no-cache build-base

ENV GALAXY_TELEGRAM_BOT=""
ENV MYID=""
ENV MAIN_CHAT_ID=""

CMD ["mkdir", "app"]

COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY . /app

WORKDIR /app

CMD ["python", "-m", "source.__main__"]