
@echo off
set ssh_username=alert
set ssh_hostname=192.168.1.3
set "project_path=~/Documents/galaxy_telegram_bot"
set "executable=./deploy.sh"

ssh %ssh_username%@%ssh_hostname% -A -X -t "cd %project_path% && git pull && chmod +x %executable% && %executable%"